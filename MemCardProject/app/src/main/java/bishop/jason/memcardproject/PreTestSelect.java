package bishop.jason.memcardproject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PreTestSelect extends AppCompatActivity {
    private static String TAG = "AppCompatActivity";

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final long TWELVE_HOURS = 60 * 60 * 12;
    public static final long ONE_DAY = TWELVE_HOURS * 2;
    public static final long THREE_DAYS = ONE_DAY * 3;
    public static final long ONE_WEEK = ONE_DAY * 7;
    public static final long TWO_WEEKS = ONE_WEEK * 2;

    private PreTestAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_test_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get cards from database
//        List<Card> testCards = makeTestableList();
//        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), calculateColumns(getApplicationContext()));

        getTestableCards();
        GridView gridView = (GridView) findViewById(R.id.gridViewTestCards);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                int selectedIndex = adapter.selectedPositions.indexOf(position);
//                View selectedView = (View) adapter.getItem(position);
                if (selectedIndex > -1) {
                    adapter.selectedPositions.remove(selectedIndex);
                    // turn off highlighting
                    view.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    adapter.selectedPositions.add(position);
                    // turn on highlighting

                    view.setBackgroundColor(Color.GREEN);
                }
            }
        });
//        Cursor testing = MemCardDbContract.testing(getApplicationContext());

//        if (testing.moveToFirst()) {
//            PreTestAdapter adapter = new PreTestAdapter(getApplicationContext(), testing);
//            do {
//                String front = testing.getString(testing.getColumnIndexOrThrow(MemCardDbContract.Cards.COLUMN_FRONT));
//            } while (testing.moveToNext());


//            findViewById(R.id.textViewNoTestMsg).setVisibility(View.VISIBLE);
//        }
//        if (testCards != null) {
//            findViewById(R.id.textViewNoTestMsg).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.textViewNoTestMsg).setVisibility(View.VISIBLE);
//        }
        // sort cards based on testing needs
    }

    @Override
    public void onBackPressed() {
        adapter.closeResources();
        endActivity(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getTestableCards()) {
            adapter.notifyDataSetChanged();
        }
    }

    /** When user clicks begin test button, will begin test with selected cards or inform user that
     *  no cards have been selected
     */
    public void beginTest(View view) {
        int[] selectedItems = adapter.getSelectedItems();
        if (selectedItems.length > 0) {
            // get selected items from adapter and begin test activity
            Intent intent = new Intent(this, TestActivity.class);
            intent.putExtra(TestActivity.TEST_SELECTION, selectedItems);

            startActivity(intent);

        } else {
            Toast.makeText(getApplicationContext(), "No cards have been selected", Toast.LENGTH_SHORT).show();
        }
        endActivity(null);
    }

    /** Ends the activity when user pushes the cancel button */
    public void endActivity(View view) {
        MemCardDbContract.closeDb();
        finish();
    }


    private boolean getTestableCards() {
        GridView gridView = (GridView) findViewById(R.id.gridViewTestCards);
        Cursor testableCards = MemCardDbContract.testing(getApplicationContext());
        TextView tvNoTest = (TextView) findViewById(R.id.textViewNoTestMsg);

        if (testableCards != null && testableCards.getCount() != 0) {
            tvNoTest.setVisibility(View.GONE);
            adapter = new PreTestAdapter(getApplicationContext(), MemCardDbContract.testing(getApplicationContext()));
            gridView.setAdapter(adapter);
            return true;
        } else {
            tvNoTest.setVisibility(View.VISIBLE);
            return false;
        }
    }

    /** Calculates the maximum number of columns that will fit on the device screen */
    private int calculateColumns(Context context) {
        // get width of display
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        // inflate view and measure width
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contenVview = inflater.inflate(R.layout.pre_test_gridview_item, null, false);
        contenVview.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int itemWidth = contenVview.getMeasuredWidth();

        // column calculation
        int colNumber = (int) (dpWidth / (itemWidth / displayMetrics.density));

        if (colNumber < 1) {
            return 1;
        }

        return colNumber;
    }

}

package bishop.jason.memcardproject;

/**
 * Created by lord_ on 8/12/2017.
 */

public class SetCard {

    public static final int COLOUR_BLACK = 0;

    private String name;
    private String amount;
    private int colour;
    private int setId;

    public SetCard(int setId, String name, int amount, int colour) {
        super();
        this.setId = setId;
        this.name = name;
        this.amount = Integer.toString(amount);
        this.colour = colour;
        this.colour = COLOUR_BLACK;
    }

    public int getColour() {
        return colour;
    }

    public String getAmount() {
        return amount;
    }

    public String getName() { return name; }

    public int getSetId() { return setId; }
}

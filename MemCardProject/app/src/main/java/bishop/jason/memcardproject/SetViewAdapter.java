package bishop.jason.memcardproject;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by lord_ on 8/12/2017.
 */

public class SetViewAdapter extends CursorAdapter {

    Context context;
    private static final String TAG = "SetViewAdapter";

    public SetViewAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        this.context = context;
    }

    /* used to inflate a new View and return it */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.set_viewer_listview_row, parent, false);
    }

    /* used to bind all the data to a View */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // find views to populate
        TextView tvName = (TextView) view.findViewById(R.id.textViewName);
        TextView tvAmount = (TextView) view.findViewById(R.id.textViewAmount);
        TextView tvColour = (TextView) view.findViewById(R.id.textViewColour);
        TextView tvSetId = (TextView) view.findViewById(R.id.textViewSetId);

        // get values from cursor
        String name = cursor.getString(cursor.getColumnIndexOrThrow(MemCardDbContract.Sets.COLUMN_NAME));
        int amount = cursor.getInt(cursor.getColumnIndexOrThrow(MemCardDbContract.Sets.VIEW_COLUMN_AMOUNT));
        int colour = cursor.getInt(cursor.getColumnIndexOrThrow(MemCardDbContract.Sets.COLUMN_COLOUR));
        int setId = cursor.getInt(cursor.getColumnIndexOrThrow(MemCardDbContract.Sets._ID));

        // populate views with values
        tvName.setText(name);
        tvAmount.setText(Integer.toString(amount));
        tvColour.setText(Integer.toString(colour));
        tvColour.setBackgroundColor(colour);
        tvSetId.setText(Integer.toString(setId));
    }
}

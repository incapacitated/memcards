package bishop.jason.memcardproject;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CreateNewSet extends AppCompatActivity {
    private int setNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_set);

        // get Intent that created view and check if need to create new set or edit existing
        Intent intent = getIntent();
        setNum = intent.getIntExtra(MainActivity.CREATE_SET_NUM, -1);

        // set custom spinner to display colours
        Spinner spinner = (Spinner) findViewById(R.id.spinnerSetColours);
        ColourSpinnerAdapter adapter = new ColourSpinnerAdapter(this);
        spinner.setAdapter(adapter);

        if (setNum != -1) {
            // look up set info in database
            // set values to db values
        }
    }

    /* activated when user clicks on SAVE button */
    public void saveSet(View view) {
        // get values
        EditText editText = (EditText) findViewById(R.id.editText);
        int colour = R.color.setBlue;
        Spinner colours = (Spinner) findViewById(R.id.spinnerSetColours);
        colour = (Integer) colours.getSelectedItem();
        // check values?

        // put into db
        saveSetToDb(editText.getText().toString(), colour);

        // return to previous activity
//        Intent intent = new Intent(this, SetViewerActivity.class);
//        startActivity(intent);
        finish();
    }

    /* saves the set information in the database */
    private void saveSetToDb(String name, int colour) {
        // get writeable db
        SQLiteDatabase database = new MemCardSQLiteHelper(this).getWritableDatabase();

        // check if creating new set or editing existing
        if (setNum == -1) {

            // put values into db
            ContentValues values = new ContentValues();

            values.put(MemCardDbContract.Sets.COLUMN_NAME, name);
            values.put(MemCardDbContract.Sets.COLUMN_COLOUR, colour);

            long newRowId = database.insert(MemCardDbContract.Sets.TABLE_NAME, null, values);

            // display info to user
            Toast.makeText(this, "Set  " + name + " created at row " + newRowId, Toast.LENGTH_LONG).show();
        } else {
            // edit values in db
            // how?
        }
    }
}
